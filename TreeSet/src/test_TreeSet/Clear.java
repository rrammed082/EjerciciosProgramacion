package test_TreeSet;

import java.util.TreeSet;

public class Clear {
	public static void main(String args[]) {
		// Creamos un TreeSet vac�o
		TreeSet<String> tree = new TreeSet<String>();

		// Usamos el m�todo add() para a�adir elementos
		tree.add("Welcome");
		tree.add("To");
		tree.add("Geeks");
		tree.add("4");
		tree.add("Geeks");
		tree.add("TreeSet");

		// Mostramos el TreeSet
		System.out.println("TreeSet: " + tree);

		// Usamos el m�todo Clear para vaciar el TreeSet
		tree.clear();

		// Mostramos el Tree Set
		System.out.println("After clearing TreeSet: " + tree);
	}
}
