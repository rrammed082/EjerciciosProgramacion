package test_TreeSet;

import java.util.TreeSet;

public class PollLast {

	public static void main(String[] args) {
		TreeSet<Integer> set = new TreeSet<Integer>();

		// Agregando elementos a este conjunto
		for (int i = 10; i <= 50; i += 10)
			set.add(i);

		// Imprimiendo el contenido del conjunto
		System.out.println("Mostramos el conjunto: " + set);

		// Recuperando y eliminando el �ltimo elemento del conjunto
		System.out.println("El �ltimo elemento del conjunto: " + set.pollLast());

		// Imprimiendo el contenido del conjunto despu�s de pollLast()
		System.out.println("Mostramos el conjunto despu�s de pollLast: " + set);
	}

}
