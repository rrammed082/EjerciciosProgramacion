package test_TreeSet;

import java.util.TreeSet;

public class PollFirst {

	public static void main(String[] args) {
		// creando un TreeSet
		TreeSet<Integer> tree = new TreeSet<Integer>();

		// agregando elementos al TreeSet
		tree.add(0);
		tree.add(1);
		tree.add(2);
		tree.add(3);
		tree.add(4);
		tree.add(5);
		tree.add(6);

		// Antes de eliminar un elemento del TreeSet lo mostramos
		System.out.println("TreeSet: " + tree);

		// Se elimina el primer elemento
		System.out.println("El primer elemento m�s bajo removido es: " + tree.pollFirst());
		System.out.println("El TreeSet se queda: " + tree);
	}

}
