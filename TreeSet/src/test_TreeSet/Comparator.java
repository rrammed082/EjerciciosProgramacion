package test_TreeSet;

import java.util.TreeSet;

public class Comparator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Creamos un TreeSet vac�o de tipo entero
		TreeSet<Integer> tree_set = new TreeSet<Integer>();

		// A�adimos elementos al conjunto usando el m�todo add()
		tree_set.add(20);
		tree_set.add(24);
		tree_set.add(30);
		tree_set.add(35);
		tree_set.add(45);
		tree_set.add(50);

		// Imprimimos los elementos dentro del objeto TreeSet
		System.out.println("Los valores del Tree Set son: " + tree_set);

		// Creamos un comparador
		Comparator comp = (Comparator) tree_set.comparator();

		// Imprimimos y mostramos los valores del comparador
		System.out.println("Ya que el valor del comparador es: " + comp);

		// Mostramos un mensaje �nicamente
		System.out.println("Sigue el orden natural");
	}

}
