package test_TreeSet;

import java.util.TreeSet;

public class Lower {

	public static void main(String[] args) {
		// Creamos un TreeSet vac�o
		TreeSet<Integer> tree = new TreeSet<Integer>();

		// Usamos el m�todo add() para rellenarlo
		tree.add(10);
		tree.add(5);
		tree.add(8);
		tree.add(1);
		tree.add(11);
		tree.add(3);

		// Mostramos el valor del TreeSet menor que el n�mero que le pasemos por par�metro
        System.out.println(tree.lower(14));

	}

}
