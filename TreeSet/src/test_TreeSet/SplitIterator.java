package test_TreeSet;

import java.util.Spliterator;
import java.util.TreeSet;

public class SplitIterator {

	public static void main(String[] args) {
		
		TreeSet<String> tree = new TreeSet<>();

		// Agregando elementos al TreeSet
		tree.add("Bienvenidos");
		tree.add("A");
		tree.add("Conocer");
		tree.add("TreeSet");

	    // Creando un Spliterator
	    Spliterator<String> spliterator = tree.spliterator();

	    // Dividiendo el Spliterator
	    Spliterator<String> newSpliterator = spliterator.trySplit();

	    System.out.println(spliterator);
	    // Mostrando los Spliterators
	    System.out.println("Spliterator:");
	    spliterator.forEachRemaining(n -> System.out.println(n));
	    System.out.println("Nuevo Spliterator:");
	    newSpliterator.forEachRemaining(n -> System.out.println(n));
	}

}
