package test_TreeSet;

import java.util.TreeSet;

public class Remove {

	public static void main(String[] args) {
		TreeSet<String> tree = new TreeSet<String>();

		// Usando el m�todo add() para agregar elementos al conjunto
		tree.add("Bienvenidos");
		tree.add("A");
		tree.add("Conocer");
		tree.add("TreeSet");

	    // Mostrando el TreeSet
	    System.out.println("TreeSet: " + tree);

	    // Elimiando elementos usando el m�todo remove()
	    tree.remove("Bienvenidos");
	    tree.remove("A");
	    tree.remove("Conocer");

	    // Mostrando el TreeSet despu�s de la eliminaci�n
	    System.out.println("Nuevo TreeSet: " + tree);
	}

}
