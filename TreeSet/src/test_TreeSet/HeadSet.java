package test_TreeSet;

import java.util.Iterator;
import java.util.TreeSet;

public class HeadSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Crear un TreeSet vac�o
		// declarando un objeto de la clase TreeSet
		TreeSet<Integer> tree_set = new TreeSet<Integer>();

		// Agregando los elementos
		// usando el m�todo add()
		tree_set.add(1);
		tree_set.add(2);
		tree_set.add(3);
		tree_set.add(4);
		tree_set.add(5);
		tree_set.add(10);
		tree_set.add(20);
		tree_set.add(30);
		tree_set.add(40);
		tree_set.add(50);

		// Creando el headSet tree
		TreeSet<Integer> head_set = new TreeSet<Integer>();

		// Limitando los valores hasta 5
		head_set = (TreeSet<Integer>) tree_set.headSet(30);

		// Creando un iterador
		Iterator<Integer> iterate;
		iterate = head_set.iterator();

		// Mostrando los datos del tree set
		System.out.println("Los valores resultantes hasta el head set: ");

		// Se mantiene verdadero mientras quede un solo elemento
		// restante en el objeto
		while (iterate.hasNext()) {

			// Iterando a trav�s del headSet
			// usando el m�todo next()
			System.out.println(iterate.next() + " ");
		}
	}

}
