package test_TreeSet;

import java.util.TreeSet;

public class AddAll {

	public static void main(String[] args) {
		// Creamos un TreeSet vac�o
        TreeSet<String> tree = new TreeSet<String>();
 
        // Usamos el m�todo add para a�adir elementos al TreeSet
        tree.add("Welcome");
        tree.add("To");
        tree.add("Geeks");
        tree.add("4");
        tree.add("Geeks");
        tree.add("TreeSet");
 
        // Mostramos el TreeSet
        System.out.println("TreeSet: " + tree);
 
        // Creamos otro TreeSet
        TreeSet<String> tree_two = new TreeSet<String>();
 
        // Usamos el m�todo add para a�adir elementos al TreeSet
        tree_two.add("Hello");
        tree_two.add("World");
 
        // Usamos el m�todo addAll() para juntar los dos TreeSet
        tree.addAll(tree_two);
 
        // Mostramos el TreeSet final
        System.out.println("TreeSet: " + tree);
    }

}
