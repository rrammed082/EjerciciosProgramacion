package test_TreeSet;

import java.util.TreeSet;

public class IsEmpty {

	public static void main(String args[]) {

		// Creamos un TreeSet vac�o
		TreeSet<String> tree = new TreeSet<String>();

		// Usamos el m�todo add() para a�adir elementos en el Set.
		tree.add("Bienvenidos");
		tree.add("A");
		tree.add("Conocer");
		tree.add("TreeSet");

		// Mostramos el TreeSet
		System.out.println("TreeSet: " + tree);

		// Comprobamos si el Set est� vac�o
		System.out.println("�Est� el SET vac�o? " + tree.isEmpty());

		// Vaciamos el TreeSet con el m�todo clear
		tree.clear();

		// Volvemos a comprobar si est� vac�o
		System.out.println("�Est� el SET vac�o? " + tree.isEmpty());
	}
}
