package test_TreeSet;

import java.util.TreeSet;

public class Iterator {

	public static void main(String[] args) {
		// Creamos un TreeSet vac�o
        TreeSet<String> tree = new TreeSet<String>();
  
        // Usamos el m�todo add() para rellenarlo
        tree.add("Bienvenidos");
		tree.add("A");
		tree.add("Conocer");
		tree.add("TreeSet");
  
        // Mostrando el treeSet
        System.out.println("TreeSet: " + tree);
  
        // Creamos un Iterador
        java.util.Iterator<String> iterator =  tree.iterator();
  
        // Mostramos los valores despu�s de iterar el TreeSet
        System.out.println("Valores del Iterador: ");
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
	}

}
