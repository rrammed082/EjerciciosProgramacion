package test_TreeSet;

import java.util.Iterator;
import java.util.TreeSet;

public class DescendingIterator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Creamos un objeto TreeSet
		TreeSet<Integer> treeadd = new TreeSet<Integer>();

		// Poblamos el TreeSet usando el m�todo add()
		treeadd.add(10);
		treeadd.add(20);
		treeadd.add(30);
		treeadd.add(40);

		// Imprimimos el TreeSet
		System.out.println("TreeSet: " + treeadd);

		// Creamos un iterador descendente
		// usando el m�todo descendingIterator()
		Iterator<Integer>
		iterator = treeadd.descendingIterator();

		System.out.println("\nValores usando el iterador descendente:");

		// Imprimimos los valores en orden descendente
		while (iterator.hasNext()) {
		System.out.println("Valor: "
		+ iterator.next());
		}
	}

}
