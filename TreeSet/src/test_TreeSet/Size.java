package test_TreeSet;

import java.util.TreeSet;

public class Size {

	public static void main(String[] args) {
		// Creando un TreeSet vac�o
		TreeSet<String> tree = new TreeSet<String>();

		// Usando el m�todo add() para agregar elementos al conjunto
		tree.add("Bienvenidos");
		tree.add("A");
		tree.add("Conocer");
		tree.add("TreeSet");

		// Mostrando el TreeSet
		System.out.println("TreeSet: " + tree);

		// Mostrando el tama�o del conjunto
		System.out.println("El tama�o del conjunto es: " + tree.size());
	}

}
