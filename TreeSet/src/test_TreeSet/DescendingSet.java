package test_TreeSet;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;


public class DescendingSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Creamos un objeto TreeSet
		TreeSet<String> treeadd = new TreeSet<String>();

		// Poblamos el TreeSet usando el m�todo add()
		treeadd.add("A");
		treeadd.add("B");
		treeadd.add("C");
		treeadd.add("D");

		// Imprimimos el TreeSet
		System.out.println("TreeSet: " + treeadd);

		// Obtenemos una vista de los elementos en orden inverso
		// usando el m�todo descendingSet()
		NavigableSet<String> treereverse = treeadd.descendingSet();

		// Obtenemos una vista iterada de NavigableSet
		Iterator<String> iterator = treereverse.iterator();

		System.out.println("\nValores usando DescendingSet:");

		// Imprimimos los valores integrados
		while (iterator.hasNext()) {
		System.out.println("Valor: " + iterator.next());
		}
	}

}
