package test_TreeSet;

import java.util.TreeSet;

public class TailSet {

	public static void main(String[] args) {
		// Creando un TreeSet vac�o
		TreeSet<Integer> tree_set = new TreeSet<Integer>();

		// A�adiendo los elementos usando add()
		tree_set.add(1);
		tree_set.add(2);
		tree_set.add(3);
		tree_set.add(4);
		tree_set.add(5);
		tree_set.add(10);
		tree_set.add(20);
		tree_set.add(30);
		tree_set.add(40);
		tree_set.add(50);

		// Creando el tailSet del �rbol
		TreeSet<Integer> tail_set = new TreeSet<Integer>();

		// Limitando los valores hasta el 5
		tail_set = (TreeSet<Integer>) tree_set.tailSet(10);

	
		// Mostrando los datos del �rbol TreeSet
		System.out.println("Los valores resultantes desde la cola: ");
		System.out.println(tail_set);
	}

}
