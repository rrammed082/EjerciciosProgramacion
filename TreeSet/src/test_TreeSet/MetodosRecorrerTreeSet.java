package test_TreeSet;

import java.util.Iterator;
import java.util.TreeSet;

public class MetodosRecorrerTreeSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub0
		
		// Creaci�n de un Tree Set
		TreeSet<Integer> treeSet = new TreeSet<>();
		treeSet.add(1);
		treeSet.add(2);
		treeSet.add(3);

		// Iterator
		// Este c�digo tambi�n imprimir� los elementos del TreeSet en orden ascendente: 1, 2, 3
		Iterator<Integer> iterator = treeSet.iterator();
		while (iterator.hasNext()) {
			Integer element = iterator.next();
			System.out.println(element);
		}

		// For Each
		// Este c�digo tambi�n imprimir� los elementos del TreeSet en orden ascendente: 1, 2, 3
		for (Integer element : treeSet) {
			System.out.println(element);

		}
	}

}
