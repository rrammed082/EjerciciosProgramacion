package test_TreeSet;

import java.util.TreeSet;

// Java program to illustrate the
// TreeSet higher() method

public class Higher {

	public static void main(String args[]) {
		TreeSet<Integer> tree = new TreeSet<Integer>();
		tree.add(10);
		tree.add(5);
		tree.add(8);
		tree.add(1);
		tree.add(11);
		tree.add(3);

		System.out.println("El �rbol es:\n" + tree);
		System.out.println("Sacamos el valor m�s alto cerca de n�mero 10: " + tree.higher(10));
	}
}
