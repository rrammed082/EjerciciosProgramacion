package test_TreeSet;

import java.util.TreeSet;

public class First {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Creando un TreeSet vac�o
		TreeSet<Integer> tree = new TreeSet<Integer>();

		// Usar el m�todo add() para agregar elementos al conjunto
		tree.add(14);
		tree.add(8);
		tree.add(200);
		tree.add(48);
		tree.add(7);
		tree.add(124);

		// Mostrar el TreeSet
		System.out.println("TreeSet: " + tree);

		// Mostrar el elemento m�s bajo del conjunto
		System.out.println("El primer elemento es: " + tree.first());
	}

}
