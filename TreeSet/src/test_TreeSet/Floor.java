package test_TreeSet;

import java.util.TreeSet;

public class Floor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// crear objeto de TreeSet
		TreeSet<Integer> treeadd = new TreeSet<Integer>();

		// poblar el TreeSet usando el m�todo add()
		treeadd.add(10);
		treeadd.add(20);
		treeadd.add(30);
		treeadd.add(40);

		// Imprimir el TreeSet
		System.out.println("TreeSet: " + treeadd);

		// obtener el valor piso para 25
		// usando el m�todo floor()
		int value = treeadd.floor(25);

		// imprimir el valor piso
		System.out.println("Valor piso para 25: " + value);
	}

}
