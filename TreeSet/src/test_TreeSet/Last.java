package test_TreeSet;

import java.util.TreeSet;

public class Last {

	public static void main(String[] args) {
		// Creamos un TreeSet vac�o
        TreeSet<Integer> tree = new TreeSet<Integer>();
  
        // Usamos el m�todo add() para rellenarlo
        tree.add(14);
        tree.add(8);
        tree.add(200);
        tree.add(48);
        tree.add(7);
        tree.add(124);
  
        // Mostramos el treeSet
        System.out.println("TreeSet: " + tree);
  
        // Displaying the highest element of the set
        System.out.println("El �ltimo y mayor elemento es: " + tree.last());
	}

}
