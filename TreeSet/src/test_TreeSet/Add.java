
package test_TreeSet;

import java.util.TreeSet;

public class Add {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creamos un TreeSet vac�o
		TreeSet<String> tree = new TreeSet<String>();

		// Usamos el m�todo add para a�adir elementos al TreeSet
		tree.add("Welcome");
		tree.add("To");
		tree.add("Geeks");
		tree.add("4");
		tree.add("Geeks");
		tree.add("TreeSet");

		// Mostramos el TreeSet
		System.out.println("TreeSet: " + tree);

	}

}
