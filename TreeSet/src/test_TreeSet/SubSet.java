package test_TreeSet;

import java.util.TreeSet;

public class SubSet {

	public static void main(String[] args) {
		// Creando un TreeSet vac�o
		TreeSet<Integer> tree = new TreeSet<Integer>();

		// A�adiendo los elementos usando add()
		tree.add(5);
		tree.add(1);
		tree.add(50);
		tree.add(10);
		tree.add(20);
		tree.add(6);
		tree.add(20);
		tree.add(18);
		tree.add(9);
		tree.add(30);

		// Creando el subset del �rbol
		TreeSet<Integer> sub_set = new TreeSet<Integer>();

		// Limitando los valores entre el 6 (incluido) y el 18(escluido)
		sub_set = (TreeSet<Integer>) tree.subSet(6, 18);

		// Mostrando los datos del �rbol TreeSet
		System.out.println("Los valores resultantes dentro del subconjunto: ");
		System.out.println(sub_set);

	}
}
