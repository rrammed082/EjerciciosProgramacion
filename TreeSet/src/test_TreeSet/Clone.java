package test_TreeSet;

import java.util.TreeSet;

public class Clone {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// Creamos un TreeSet vac�o
		TreeSet<String> tree = new TreeSet<>();

		// Usamos add() para a�adir elementos
		tree.add("Bienvenido");
		tree.add("A");
		tree.add("Geeks");
		tree.add("4");
		tree.add("Geeks");
		tree.add("TreeSet");

		// Mostramos el TreeSet
		System.out.println("TreeSet: " + tree);

		// Creamos un nuevo conjunto clonado
		TreeSet<String> cloned_set = new TreeSet<>();

		// Clonamos el conjunto usando el m�todo clone()
		cloned_set = (TreeSet<String>) tree.clone();

		// Mostramos el conjunto clonado
		System.out.println("El TreeSet clonado: " + cloned_set);
	}

}
