package test_TreeSet;

import java.util.TreeSet;

public class Contains {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Creamos un TreeSet vac�o de tipo cadena
		TreeSet<String> tree = new TreeSet<String>();

		// Agregamos elementos al TreeSet usando el m�todo add()
		tree.add("Bienvenido");
		tree.add("A");
		tree.add("Geeks");
		tree.add("4");
		tree.add("Geeks");
		tree.add("TreeSet");

		// Mostramos el TreeSet
		System.out.println("TreeSet: " + tree);

		// Caso de uso 1
		// Verificar si un elemento espec�fico est� en el objeto TreeSet anterior
		// usando el m�todo contains() de la clase TreeSet

		// Imprimiendo un valor booleano
		System.out.println("�El conjunto contiene 'TreeSet'? " + tree.contains("TreeSet"));

		// Caso de uso 2
		// Verificar si un elemento espec�fico est� en el objeto TreeSet anterior
		// Supongamos que el elemento personalizado sea "4"
		System.out.println("�El conjunto contiene '4'? " + tree.contains("4"));

		// Caso de uso 3
		// Verificar si la lista contiene "No"
		System.out.println("�El conjunto contiene 'No'? " + tree.contains("No"));
	}

}
