package test_TreeSet;

import java.util.TreeSet;

public class Ceiling {

	public static void main(String[] args) {
		
		// Creamos un TreeSet Vac�o
		TreeSet<Integer> treeadd = new TreeSet<Integer>();

		// Usamos el m�todo add para a�adir elementos al TreeSet
		treeadd.add(10);
		treeadd.add(20);
		treeadd.add(30);
		treeadd.add(40);

		// Mostramos el TreeSet
		System.out.println("TreeSet: " + treeadd);

		// Usamos el m�todo ceiling() para obtener el n�mero superior al introducido por par�metro
		int value = treeadd.ceiling(25);

		// Mostramos el valor de ceiling()
		System.out.println("N�mero superior a 25: " + value);

	}

}
